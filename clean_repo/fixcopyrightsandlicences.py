#!/usr/bin/python


# Author: edward.moyse@cern.ch
# (with lots of inspiration/code from svnpull by Graeme Stewart)
# Copyright (C) 2018 CERN for the benefit of the ATLAS collaboration

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os,pprint,json,re
import fnmatch
import difflib
import logging
logger = logging.getLogger('fixcopyrightandlicences')
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)

# logger.setLevel(logging.WARNING)

gpl_licence = [
"""//***************************************************************************""",
"""//  *                                                                         *""",
"""This program is free software; you can redistribute it and/or modify""",
"""it under the terms of the GNU General Public License as published by""",
"""the Free Software Foundation; either version 2 of the License, or""",
"""(at your option) any later version.""",
"""//  *                                                                         *""",
"""//  ***************************************************************************/"""]

    
atlas_copyright="Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration"
old_atlas_copyright="Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration"
extensions = (".cxx", ".cpp", ".icc", ".cc", ".c", ".C", ".h", ".hpp", ".hh") # Lifted from Graeme's script

def scan_path(args, license_path_accept, license_path_reject):
    logger.info('Scanning path : '+args.path)
    
    report = {'gpl_complete_match':[],# perfect match
            'gpl_partial_match':[],# GPL mentioned but didn't get complete match
            'licenced':[],
            'private_copyright':[],
            'missing_atlas_copyright':[],
            'old_atlas_copyright':[]}

    for subdir, dirs, files in os.walk(args.path):
        for filename in files:  
            path_veto = False
            
            # print subdir
            relpath = os.path.relpath(subdir, subdir[:subdir.find('athena')+7]) # Let's get path relative to repo root.
            
            path = os.path.join(relpath,filename)
            for filter in license_path_reject:
                if re.match(filter, path):
                    logger.debug("File {0} will not have a license file applied".format(path, filter.pattern))
                    path_veto = True
                
            if not path_veto:
                check_file(subdir,filename, report, args)
    if args.checklicences:
        print '+ Found',len(report['gpl_complete_match']),' files with GPL:', report['gpl_complete_match']
        print '+ Found',len(report['gpl_partial_match']),' files with partial GPL matches:', report['gpl_partial_match']
        print '+ Found',len(report['licenced']),' files containing the word licence/license (and not in above):', report['licenced']
    if args.checkcopyright:
        print '+ Found',len(report['private_copyright']),' files with private copyright:', report['private_copyright']
        print '+ Found',len(report['missing_atlas_copyright']),' files missing ATLAS copyright:', report['missing_atlas_copyright']
    if args.oldcheckcopyright:
        print '+ Found',len(report['old_atlas_copyright']),' files with old ATLAS copyright:', report['old_atlas_copyright']
            
def check_file(subdir,filename, report, args):
    f = open(subdir+'/'+filename,'r')
    lines=f.readlines()
    
    # We could optimise this because it doesn't make so much sense to scan each file twice, but this is done once
    exclude_these_linenumbers = []
    if args.checklicences:
        exclude_these_linenumbers+= check_licence(filename, subdir, lines, report,args) 
    
    debug=False
    if filename=='cleanup.csh' or filename=='TRProcessRun.mac':
        debug=True
    
    if args.checkcopyright and filename.endswith(extensions):
        exclude_these_linenumbers+= check_copyright(filename, subdir, lines, report, args)             
    
    if args.fix_files:
        offset = 0 # bit ugly, but *IF* we insert the copyright, we need to take account of this
        if filename.endswith(extensions) and args.checkcopyright:
            if debug:
                print 'WTF'
            # Only try to insert copyright in certain files 
            if report['missing_atlas_copyright'] and report['missing_atlas_copyright'][-1]==filename:
                # Okay, so we added this file to the list of files missing copyright,  
                insert_atlas_copyright(filename, lines)
                offset = 3 # to account for inserted lines
            
        if exclude_these_linenumbers>0:
            target_filename = subdir+'/'+filename + ".temp"
            ofh = open(target_filename, "w")
              
            for index, line in enumerate(lines):
                if index-offset not in exclude_these_linenumbers:
                    ofh.write(line)
            ofh.close()
            
            fmode = os.stat(subdir+'/'+filename).st_mode # Need this to restore the mode later on.
                
            os.rename(target_filename, subdir+'/'+filename)
            os.chmod(subdir+'/'+filename, fmode)
                
            # print 'lines', lines

def insert_atlas_copyright(filename, lines):
    insert_index=0
    first_line=lines[0]
    if re.search(r"-\*-\s+[cC]\+\+\s+-\*\-", first_line):
        # special comment on first line
        # print first_line
        if '/*' in first_line and not '*/' in first_line:
            logger.error(filename+' : Not handling multiline emacs instructions yet!')
        insert_index=1
    lines.insert(insert_index, '*/\n')
    lines.insert(insert_index, '  '+atlas_copyright+'\n')
    lines.insert(insert_index,'/*\n')

def check_licence(filename, subdir, lines, report, args):
    gpl_matching_lines=0
    found_complete_gpl=False
    found_partial_gpl=False
    licence_mentioned = False
    
    exclude_these_linenumbers = []
    
    debug=False
    if filename=="XXXX":
        debug=True
    
    for index, line in enumerate(lines):
        skip_line = False
        if 'licence' in line.lower() or 'license' in line.lower():
            licence_mentioned=True
            # we're not necessarily stripping this line.
        
        if index<10 and debug:
            print line
        # Explicitly look for GPL
        if not gpl_matching_lines and gpl_licence[2] in line:
            # Match some interesting code first, rather than just asterixes
            exclude_these_linenumbers.append(index)
            gpl_matching_lines=3 
            
            if index>1 and gpl_licence[1] in lines[index-1]:
                exclude_these_linenumbers.append(index-1)
            if index>2 and gpl_licence[0] in lines[index-2]:
                exclude_these_linenumbers.append(index-2)
            if found_complete_gpl:
                logger.warning('Already found gpl licence in file ['+filename+'] but mentioned again!')
        elif gpl_matching_lines:
            if gpl_licence[gpl_matching_lines] in line:
                gpl_matching_lines+=1
                if gpl_matching_lines==len(gpl_licence):
                    found_complete_gpl=True
                    gpl_matching_lines=0
                exclude_these_linenumbers.append(index)
            else:
                found_partial_gpl=True
                logger.debug('Partial match for ['+filename+' ] failing on this line: '+line)
        
    if found_complete_gpl:
        report['gpl_complete_match'].append(filename)
                  
    if found_partial_gpl:
        report['gpl_partial_match'].append(filename)
    
    if found_partial_gpl and found_complete_gpl:
        logger.error('Found both partial and complete gpl for '+filename)
        
    if licence_mentioned and not found_complete_gpl:
        # Some other licence
        report['licenced'].append(filename)
        
    return exclude_these_linenumbers

# Heavily influenced by inject_c_license from svnpull by Graeme
def check_copyright(filename,subdir, lines,report,args):        
    exclude_these_linenumbers = []
    
    debug=False
    if filename=="XXXXX":
        debug=True
    
    has_copyright=False
    has_atlas_copyright=False
    for index, line in enumerate(lines):
        if 'copyright' in line.lower():
            has_copyright=True
            if line.rstrip().lstrip() == atlas_copyright:
                has_atlas_copyright=True
            else:
                if old_atlas_copyright in line: #bit more relaxed for this... don't want to change files for whitespace
                    if args.oldcheckcopyright:
                        report['old_atlas_copyright'].append(filename)
                        if args.fix_files:
                            # okay, this is a little bit hacky ...
                            lines[index] = atlas_copyright+'\n'
                    else:
                        has_atlas_copyright=True
                else:
                    # logger.info( 'Found file ' + filename + ' with private copyright:')
                    # logger.info( 'current: '+line )
                    # logger.info( 'atlas  : '+atlas_copyright )
                    # logger.info( 'atlas17: '+old_atlas_copyright )
                    # for i,s in enumerate(difflib.ndiff(line, atlas_copyright)):
                    #         if s[0]==' ': continue
                    #         elif s[0]=='-':
                    #             print(u'Delete "{}" from position {}'.format(s[-1],i))
                    #         elif s[0]=='+':
                    #             print(u'Add "{}" to position {}'.format(s[-1],i))
                    
                    report['private_copyright'].append(filename)
                    if args.check_private_copyright:
                        exclude_these_linenumbers.append(index)

    if not has_atlas_copyright:
        report['missing_atlas_copyright'].append(filename)
    return exclude_these_linenumbers

# Lifted from svnpull by Graeme!
def inject_c_license(filename, license_text):
    ## @brief Add a license file, C style commented
    target_filename = filename + ".copyright"
    with open(filename) as ifh, open(target_filename, "w") as ofh:
        first_line = ifh.readline()
        # If the first line is a -*- C++ -*- then it has to stay the
        # first line
        if re.search(r"-\*-\s+[cC]\+\+\s+-\*\-", first_line):
            multi_line_c_comment = False
            # Beware of breaking a multi-line C style comment
            if first_line.startswith("/*") and ("*/" not in first_line[2:]):
                first_line = first_line[:-1] + " */\n"
                multi_line_c_comment = True
            ofh.write(first_line)
            ofh.write("\n/*\n")
            for line in license_text:
                ofh.write("  {0}\n".format(line)) if line != "" else ofh.write("\n")
            ofh.write("*/\n\n")
            if multi_line_c_comment:
                ofh.write("/*\n")
        else:
            ofh.write("/*\n")
            for line in license_text:
                ofh.write("  {0}\n".format(line)) if line != "" else ofh.write("\n")
            ofh.write("*/\n\n")
            ofh.write(first_line)
        for line in ifh:
            ofh.write(line)
    os.rename(target_filename, filename)

def load_exceptions_file(filename, reject_changelog=False):
    ## @brief Parse and return path globbing exceptions file
    #  @param filename File containing exceptions
    #  @param reject_changelog Special flag used by svnpull to ensure that
    #  ChangeLog files are rejected (in a normal svn2git they are accepted,
    #  onto the import branches, but then excluded specially from the
    #  release branches)
    #  @return Tuple of path globs to accept and globs to reject, converted to regexps
    path_accept = []
    path_reject = []
    if filename != "NONE":
        with open(filename) as filter_file:
            logger.info("Loaded import exceptions from {0}".format(filename))
            for line in filter_file:
                line = line.strip()
                if reject_changelog and ("ChangeLog" in line):
                    logger.debug("Found ChangeLog line, which will be forced to reject: {0}".format(line))
                    line = "- */ChangeLog"
                if line.startswith("#") or line == "":
                    continue
                if line.startswith("-"):
                    path_reject.append(re.compile(fnmatch.translate(line.lstrip("- "))))
                else:
                    path_accept.append(re.compile(fnmatch.translate(line.lstrip("+ "))))
    logger.debug("Glob accept: {0}".format([ m.pattern for m in path_accept ]))
    logger.debug("Glob reject: {0}".format([ m.pattern for m in path_reject ]))
    return path_accept, path_reject


def main():
    parser = argparse.ArgumentParser(description='Script to examine ATLAS source code for correct licences and copyright, and fix them if requested.')
    parser.add_argument('--debug', '--verbose', "-v", action="store_true",
                        help="Switch logging into DEBUG mode (default is WARNING)")
    parser.add_argument('--path',
                        help="Path to search (default is the current directory).", default='.')
    parser.add_argument('--checkcopyright', action="store_true",
                        help="Check for correct ATLAS copyright.")
    parser.add_argument('--checklicences', action="store_true",
                        help="Check for private licence declarations in files")
    parser.add_argument('--oldcheckcopyright', action="store_true",
                        help="Count for older ATLAS copyright (false by default).")
    parser.add_argument('--check-private-copyright', action="store_true",
                        help="Strip private copyright statements if found")
    parser.add_argument('--fix-files', action="store_true",
                        help="Actually change files (default is not to change files)")
    parser.add_argument('--licenseexceptions', metavar="FILE", help="File listing path globs to exempt from or  "
                        "always apply license file to (same format as --svnfilterexceptions). "
                        "It is strongly recommended to keep the default value to ensure consistency "
                        "with the official ATLAS migration.",
                        default=os.path.join(os.path.dirname(os.path.abspath(__file__)), "../atlaslicense-exceptions.txt"))
    
    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    
    logger.info('About to run with path: ' + args.path)
    logger.info('The configuration is the following: ')
    if args.checkcopyright:
        logger.info('- will check copyright')
    if args.checklicences:
        logger.info('- will check licences')
    if args.oldcheckcopyright:
        logger.info('- will look for old ATLAS copyright statements')
    if args.check_private_copyright:
        logger.info('- will check for private copyright statements')
    if args.fix_files:
        logger.info('- will fix files in place if the above issues are detected')
    logger.info('\n')    
    license_path_accept, license_path_reject = load_exceptions_file(args.licenseexceptions)
    logger.info('\n')    
    
    scan_path(args, license_path_accept, license_path_reject)

if __name__ == '__main__':
    main()
