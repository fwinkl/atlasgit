#!/usr/bin/env python
"""Fix file permission for files with same git hash.

Files with identical content (same SHA1) but different file permissions
cause confusing differences shown in GitLab. This script changes the permissions
of identical files to 644. Note that git only keeps track of the user's executable
bit, i.e. 755 and 644 are the only relevant modes. The script needs to be run from
a checkout of the relevant branch and the changes commited manually.

See also: https://its.cern.ch/jira/browse/ATLINFR-2011
"""

__author__ = "Frank Winklmeier"

import sys
import os
import argparse
import subprocess

def git_lstree():
   out = subprocess.check_output(['git','ls-tree','-r','HEAD'])
   # 100644 blob 7165f46b8ad9c0bdabc453bd7edb59428c247214   Control/StoreGateTests/python/__init__.py
   return [l.split() for l in out.splitlines()]

def main():
   parser = argparse.ArgumentParser(description=__doc__)
   parser.add_argument('--dry-run', action='store_true', help='Do not apply permission changes')
   args = parser.parse_args()

   lstree = git_lstree()

   hash_perm = {}   # Dictionary hash -> [permissions]
   hash_name = {}   # Dictionary hash -> [names]
   for f in lstree:
      # Skip "aliases"
      if len(f)>4 and f[4]=='alias': continue
      hash_perm.setdefault(f[2], set()).add(f[0])
      hash_name.setdefault(f[2], []).append(f[3])

   # Hashes with permission conflicts
   conflicts = [k for k,v in hash_perm.iteritems() if len(v)>1]
   for f in lstree:
      if f[2] in conflicts and f[0]=='100755':
         print('chmod 644 %s' % f[3])
         if not args.dry_run:
            os.chmod(f[3], 0o644)

if __name__ == '__main__':
   sys.exit(main())
