#!/usr/bin/bash

# Assuming you will run this in a directory which contains atlasgit 
# i.e. the command you'll type will be:
# atlasgit/clean_repo/cleanrepo.sh

CURRDIR=`pwd`
date
echo
echo ++++ Starting repository cleanup in $CURRDIR
echo ++++ 1. Copying BFG locally.
echo
wget http://repo1.maven.org/maven2/com/madgag/bfg/1.13.0/bfg-1.13.0.jar

echo ++++ 2. Getting bare repo
echo 
# git clone --mirror ssh://git@gitlab.cern.ch:7999/atlas/athena.git
# Temporarily use local clone
git clone --mirror ../athena

echo ++++ 3. Trying to remove large files, using BFG.
echo
java -jar bfg-1.13.0.jar --strip-blobs-bigger-than 100M athena.git
cd athena.git
echo Now cleanup.
git reflog expire --expire=now --all && git gc --prune=now --aggressive
cd ..

echo ++++ 4. Trying to remove inappropriate language, using BFG.
echo
java -jar bfg-1.13.0.jar  --replace-text atlasgit/clean_repo/bannedphrases.txt  athena.git
echo Now cleanup.
cd athena.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
cd ..

# This directory needs to be removed:
# Tracking/TrkAlignment/TrkAlgebraUtils/MA27
echo ++++ 5. Remove directory we want gone.
java -jar bfg-1.13.0.jar --delete-folders MA27 athena.git 
echo Now cleanup.
cd athena.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
cd ..

echo ++++ 6. Get working copy from the bare repo
echo
mkdir LocalCopy
cd LocalCopy
git clone ../athena.git
cd athena

echo ++++ 7. Time for filter branch, to stomp history
# Now, time for filterbranch
FILEDIR=$CURRDIR/atlasgit/clean_repo/stomp_history
# Now fix problematic files by copying local version over version in repo
# git filter-branch -f --index-filter "
#   for LINE in `cat $CURRDIR/atlasgit/clean_repo/stomp_history/list.txt` ;
#   do
#     echo $LINE;
#     FILE=`basename $LINE` ;
#     if [ -d `dirname $LINE` ];
#       then cp -f $FILEDIR/$FILE $LINE  ; git add $LINE ; fi ;
#   done " --tag-name-filter cat -- --all
git filter-branch -f --index-filter "$FILEDIR/replace_files.sh $CURRDIR" --tag-name-filter cat -- --all

cd $CURRDIR

echo Finished
date

# At this point we're done.
# We now need to :
# git push origin --force --all
# git push origin --force --tags
# And then import the repository to gitlab, after renaming the existing repository to athena-private1


