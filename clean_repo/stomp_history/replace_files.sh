#!/usr/bin/bash
CURRDIR=$1

FILEDIR=$CURRDIR/atlasgit/clean_repo/stomp_history

for LINE in `cat $CURRDIR/atlasgit/clean_repo/stomp_history/list.txt` ;
do
  # echo $LINE;
  FILE=`basename $LINE` ;
  if [ -d `dirname $LINE` ];
    then cp -f $FILEDIR/$FILE $LINE  ; git add $LINE ; fi ;
done 